package ru.ahmetahunov.sp.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.AdminEndpoint;
import ru.ahmetahunov.sp.api.endpoint.Role;
import ru.ahmetahunov.sp.command.AbstractCommand;
import ru.ahmetahunov.sp.util.RoleUtil;

@NoArgsConstructor
@Component("change-user-access")
public final class UserChangeAccessAdminCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "change-user-access";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change selected user's access.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[CHANGE USER'S ACCESS RIGHTS]");
        @NotNull final String userId = terminalService.getAnswer("Please enter user's id: ");
        @NotNull final String answer = terminalService.getAnswer("Please enter role<User/Administrator>: ");
        @Nullable final Role role = RoleUtil.getRole(answer);
        adminEndpoint.userChangeRoleAdmin(session, userId, role);
        terminalService.writeMessage("[OK]");
    }

}
