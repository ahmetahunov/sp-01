package ru.ahmetahunov.sp.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.TaskEndpoint;
import ru.ahmetahunov.sp.command.AbstractCommand;

@NoArgsConstructor
@Component("task-clear")
public final class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all user's available tasks.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        taskEndpoint.removeAllTasks(session);
        terminalService.writeMessage("[ALL TASKS REMOVED]");
    }

}