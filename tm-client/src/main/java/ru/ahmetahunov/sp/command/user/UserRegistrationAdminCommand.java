package ru.ahmetahunov.sp.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.AdminEndpoint;
import ru.ahmetahunov.sp.command.AbstractCommand;
import ru.ahmetahunov.sp.exception.FailedOperationException;
import java.lang.Exception;

@NoArgsConstructor
@Component("user-register-admin")
public final class UserRegistrationAdminCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "user-register-admin";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "New user registration with role setting.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[REGISTRATION ADMIN]");
        @NotNull final String login = terminalService.getAnswer("Please enter login: ");
        @NotNull String password = terminalService.getAnswer("Please enter password: ");
        @NotNull final String repeatPass = terminalService.getAnswer("Please enter new password one more time: ");
        if (password.isEmpty() || !password.equals(repeatPass))
            throw new FailedOperationException("Passwords do not match!");
        adminEndpoint.userRegisterAdmin(session, login, password);
        terminalService.writeMessage("[OK]");
    }

}
