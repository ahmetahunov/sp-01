package ru.ahmetahunov.sp.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.ProjectEndpoint;
import ru.ahmetahunov.sp.command.AbstractCommand;
import java.lang.Exception;

@NoArgsConstructor
@Component("project-clear")
public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all users's available projects.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        projectEndpoint.removeAllProjects(session);
        terminalService.writeMessage("[ALL PROJECTS REMOVED]");
    }

}