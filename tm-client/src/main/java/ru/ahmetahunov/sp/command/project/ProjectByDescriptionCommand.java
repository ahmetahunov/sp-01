package ru.ahmetahunov.sp.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.*;
import ru.ahmetahunov.sp.command.AbstractCommand;
import java.lang.Exception;
import java.util.List;

@NoArgsConstructor
@Component("project-by-description")
public final class ProjectByDescriptionCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "project-by-description";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects with entered part of description.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[PROJECTS BY DESCRIPTION]");
        @NotNull final String description = terminalService.getAnswer("Please enter project description: ");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectByDescription(session, description);
        int i = 1;
        for (@NotNull final ProjectDTO project : projects) {
            terminalService.writeMessage(
                    String.format(
                            "%d.%s ID:%s\nDescription:%s",
                            i++,
                            project.getName(),
                            project.getId(),
                            project.getDescription()
                    )
            );
        }
    }

}
