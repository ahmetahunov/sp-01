package ru.ahmetahunov.sp.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.AdminEndpoint;
import ru.ahmetahunov.sp.api.endpoint.UserDTO;
import ru.ahmetahunov.sp.command.AbstractCommand;

@NoArgsConstructor
@Component("user-list")
public final class UserListAdminCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "user-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all users.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[USER LIST]");
        int i = 1;
        for (@NotNull final UserDTO user : adminEndpoint.findAllUsers(session)) {
            terminalService.writeMessage(String.format("%d. %s ID:%s", i++, user.getLogin(), user.getId()));
        }
    }

}
