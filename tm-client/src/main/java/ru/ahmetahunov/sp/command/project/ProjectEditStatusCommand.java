package ru.ahmetahunov.sp.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.*;
import ru.ahmetahunov.sp.command.AbstractCommand;
import ru.ahmetahunov.sp.exception.FailedOperationException;
import ru.ahmetahunov.sp.util.StatusUtil;

@NoArgsConstructor
@Component("project-edit-status")
public final class ProjectEditStatusCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "project-edit-status";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit status of project.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[EDIT PROJECT STATUS]");
        @NotNull final String projectId = terminalService.getAnswer("Please enter project id: ");
        @Nullable final ProjectDTO project = projectEndpoint.findOneProject(session, projectId);
        if (project == null) throw new FailedOperationException("Selected project does not exist");
        @NotNull final String statusName = terminalService.getAnswer("Enter status<planned|in-progress|done>: ");
        @Nullable final Status status = StatusUtil.getStatus(statusName);
        project.setStatus(status);
        projectEndpoint.updateProject(session, project);
        terminalService.writeMessage("[OK]");
    }

}
