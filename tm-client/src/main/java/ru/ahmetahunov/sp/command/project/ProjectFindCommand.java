package ru.ahmetahunov.sp.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.ProjectDTO;
import ru.ahmetahunov.sp.api.endpoint.ProjectEndpoint;
import ru.ahmetahunov.sp.command.AbstractCommand;
import java.util.List;

@NoArgsConstructor
@Component("project-find")
public class ProjectFindCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String getName() { return "project-find"; }

    @NotNull
    @Override
    public String getDescription() { return "Search for project by part of name or description."; }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[FIND PROJECT]");
        @NotNull final String searchPhrase =
                terminalService.getAnswer("Please enter project name or description: ");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectByNameOrDesc(session, searchPhrase);
        int i = 1;
        for (@NotNull final ProjectDTO project : projects) {
            terminalService.writeMessage(
                    String.format(
                            "%d.%s ID:%s\nDescription:%s",
                            i++,
                            project.getName(),
                            project.getId(),
                            project.getDescription()
                    )
            );
        }
        if (projects.isEmpty()) terminalService.writeMessage("Not found.");
    }

}
