package ru.ahmetahunov.sp.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.*;
import ru.ahmetahunov.sp.command.AbstractCommand;
import java.lang.Exception;
import java.util.List;

@NoArgsConstructor
@Component("task-find")
public class TaskFindCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "task-find";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Search task by part of name or description.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[FIND TASK]");
        @NotNull final String searchPhrase = terminalService.getAnswer("Please enter task name or description: ");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findTaskByNameOrDesc(session, searchPhrase);
        int i = 1;
        for (@NotNull final TaskDTO task : tasks) {
            terminalService.writeMessage(
                    String.format(
                            "%d.%s ID:%s\nDescription:%s",
                            i++,
                            task.getName(),
                            task.getId(),
                            task.getDescription())
            );
        }
        if (tasks.isEmpty()) terminalService.writeMessage("Not found.");
    }

}
