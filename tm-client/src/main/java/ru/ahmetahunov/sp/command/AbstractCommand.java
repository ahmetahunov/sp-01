package ru.ahmetahunov.sp.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ahmetahunov.sp.api.service.IStateService;
import ru.ahmetahunov.sp.api.service.ITerminalService;

@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    @Autowired
    protected ITerminalService terminalService;

    @NotNull
    @Autowired
    protected IStateService stateService;

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

}