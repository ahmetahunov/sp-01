package ru.ahmetahunov.sp.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.SessionEndpoint;
import ru.ahmetahunov.sp.command.AbstractCommand;

@NoArgsConstructor
@Component("log-in")
public final class UserLogInCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "log-in";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User log in.";
    }

    @Override
    public void execute() throws Exception{
        terminalService.writeMessage("[LOG IN]");
        @NotNull final String login = terminalService.getAnswer("Please enter login: ");
        @NotNull final String password = terminalService.getAnswer("Please enter password: ");
        @Nullable String session = sessionEndpoint.createSession(login, password);
        stateService.setSession(session);
        terminalService.writeMessage("Welcome, " + login + "!");
    }

}
