package ru.ahmetahunov.sp.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.UserEndpoint;
import ru.ahmetahunov.sp.command.AbstractCommand;
import ru.ahmetahunov.sp.exception.FailedOperationException;

@NoArgsConstructor
@Component("user-change-pass")
public final class UserChangePassCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "user-change-pass";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change password.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[CHANGE PASSWORD]");
        @NotNull final String oldPassword = terminalService.getAnswer("Please enter old password: ");
        @NotNull final String password = terminalService.getAnswer("Please enter new password: ");
        @NotNull final String repeatPass = terminalService.getAnswer("Please enter new password one more time: ");
        if (password.isEmpty() || !password.equals(repeatPass))
            throw new FailedOperationException("Passwords do not match!");
        userEndpoint.updatePassword(session, oldPassword, password);
        terminalService.writeMessage("[OK]");
    }

}
