package ru.ahmetahunov.sp.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.ProjectEndpoint;
import ru.ahmetahunov.sp.command.AbstractCommand;

@NoArgsConstructor
@Component("project-remove")
public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[PROJECT REMOVE]");
        @NotNull final String projectId = terminalService.getAnswer("Please enter project id: ");
        projectEndpoint.removeProject(session, projectId);
        terminalService.writeMessage("[OK]");
    }

}