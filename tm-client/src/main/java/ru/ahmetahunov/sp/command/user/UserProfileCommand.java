package ru.ahmetahunov.sp.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.UserDTO;
import ru.ahmetahunov.sp.api.endpoint.UserEndpoint;
import ru.ahmetahunov.sp.command.AbstractCommand;
import ru.ahmetahunov.sp.util.InfoUtil;

@NoArgsConstructor
@Component("user-profile")
public final class UserProfileCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "user-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show user's profile information.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        @Nullable final UserDTO user = userEndpoint.findUser(session);
        terminalService.writeMessage("[USER PROFILE]");
        terminalService.writeMessage(InfoUtil.getUserInfo(user));
    }

}
