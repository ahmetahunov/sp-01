package ru.ahmetahunov.sp.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.ProjectDTO;
import ru.ahmetahunov.sp.api.endpoint.ProjectEndpoint;
import ru.ahmetahunov.sp.command.AbstractCommand;
import ru.ahmetahunov.sp.exception.FailedOperationException;
import ru.ahmetahunov.sp.util.DateUtil;

@NoArgsConstructor
@Component("project-create")
public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[PROJECT CREATE]");
        @NotNull final String name = terminalService.getAnswer("Please enter project name: ");
        if (name.isEmpty()) throw new FailedOperationException("Name cannot be empty.");
        @NotNull final String description = terminalService.getAnswer("Please enter description:");
        @NotNull final String startDate =
                terminalService.getAnswer("Please insert start date(example: 01.01.2020): ");
        @NotNull final String finishDate =
                terminalService.getAnswer("Please enter finish date(example: 01.01.2020): ");
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setStartDate(DateUtil.parseDate(startDate));
        project.setFinishDate(DateUtil.parseDate(finishDate));
        projectEndpoint.createProject(session, project);
        terminalService.writeMessage("[OK]");
    }

}