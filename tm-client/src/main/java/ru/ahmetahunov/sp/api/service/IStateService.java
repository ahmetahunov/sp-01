package ru.ahmetahunov.sp.api.service;

import org.jetbrains.annotations.Nullable;

public interface IStateService {

    @Nullable
    public String getSession();

    public void setSession(String session);

}
