package ru.ahmetahunov.sp.context;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.service.*;
import ru.ahmetahunov.sp.command.*;
import javax.annotation.PostConstruct;
import java.lang.Exception;
import java.util.Map;

@Component
@RequiredArgsConstructor
public final class Bootstrap {

    @NotNull
    @Autowired
    private final Map<String, AbstractCommand> commands;

    @NotNull
    @Autowired
    private final ITerminalService terminalService;

    @PostConstruct
    public void init() throws Exception {
        terminalService.writeMessage( "*** WELCOME TO TASK MANAGER ***" );
        @NotNull String operation = "";
        while (!"exit".equals(operation)) {
            try {
                operation = terminalService.getAnswer("Please enter command: ").toLowerCase();
                @Nullable final AbstractCommand command = getCommand(operation);
                if (command == null) continue;
                command.execute();
            }
            catch (Exception e) { terminalService.writeMessage(e.getMessage()); }
            terminalService.writeMessage("");
        }
        terminalService.close();
    }

    @Nullable
    private AbstractCommand getCommand(@Nullable final String operation) {
        if (operation == null || operation.isEmpty()) return null;
        @Nullable final AbstractCommand command = commands.get(operation);
        if (command == null) return commands.get("unknown");
        return command;
    }

}
