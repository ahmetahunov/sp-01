package ru.ahmetahunov.sp.integration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.ahmetahunov.sp.api.endpoint.*;
import ru.ahmetahunov.sp.endpoint.AdminEndpointImplService;
import ru.ahmetahunov.sp.endpoint.ProjectEndpointImplService;
import ru.ahmetahunov.sp.endpoint.SessionEndpointImplService;
import ru.ahmetahunov.sp.endpoint.UserEndpointImplService;
import ru.ahmetahunov.sp.util.DateUtil;
import java.util.List;

public class ProjectEndpointIT {

	@NotNull
	private static final SessionEndpoint sessionEndpoint = new SessionEndpointImplService().getSessionEndpointImplPort();

	@NotNull
	private static final UserEndpoint userEndpoint = new UserEndpointImplService().getUserEndpointImplPort();

	@NotNull
	private static final AdminEndpoint adminEndpoint = new AdminEndpointImplService().getAdminEndpointImplPort();

	@NotNull
	private static final ProjectEndpoint projectEndpoint = new ProjectEndpointImplService().getProjectEndpointImplPort();

	@Nullable
	private static String tokenAdmin;

	@Nullable
	private static UserDTO user1;

	@Nullable
	private static UserDTO user2;

	@Nullable
	private static String token1;

	@Nullable
	private static String token2;

	@BeforeClass
	public static void init() throws Exception {
		user1 = userEndpoint.createUser("user1", "user1");
		user2 = userEndpoint.createUser("user2", "user2");
		token1 = sessionEndpoint.createSession("user1", "user1");
		token2 = sessionEndpoint.createSession("user2", "user2");
		tokenAdmin = sessionEndpoint.createSession("admin", "admin");
	}

	@AfterClass
	public static void clean() throws Exception {
		adminEndpoint.userRemove(tokenAdmin, user1.getId());
		adminEndpoint.userRemove(tokenAdmin, user2.getId());
		sessionEndpoint.removeSession(tokenAdmin);
	}

	@After
	public void clearProjects() throws Exception {
		projectEndpoint.removeAllProjects(token1);
		projectEndpoint.removeAllProjects(token2);
	}

	@Test
	public void createProjectExceptionTest() {
		@NotNull final ProjectDTO project = new ProjectDTO();
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.createProject(null, project)
		);
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.createProject("", project)
		);
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.createProject(token1, null)
		);
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.createProject(token1, project)
		);
	}

	@Test
	public void createProjectOkTest() throws Exception {
		@NotNull final ProjectDTO project = new ProjectDTO();
		@Nullable final ProjectDTO check = projectEndpoint.findOneProject(token1, project.getId());
		Assert.assertNull(check);
		project.setName("testProject");
		@NotNull final ProjectDTO created = projectEndpoint.createProject(token1, project);
		Assert.assertNotNull(created);
		Assert.assertEquals(user1.getId(), created.getUserId());
		@NotNull final ProjectDTO found = projectEndpoint.findOneProject(token1, created.getId());
		Assert.assertNotNull(found);
		Assert.assertEquals(created.getId(), found.getId());
		Assert.assertEquals(created.getName(), found.getName());
		Assert.assertEquals(created.getDescription(), found.getDescription());
		Assert.assertEquals(created.getCreationDate(), found.getCreationDate());
		Assert.assertEquals(created.getFinishDate(), found.getFinishDate());
		Assert.assertEquals(created.getStartDate(), found.getStartDate());
		Assert.assertEquals(created.getStatus(), found.getStatus());
		Assert.assertEquals(user1.getId(), found.getUserId());
	}

	@Test
	public void updateProjectExceptionTest() {
		@NotNull final ProjectDTO project = new ProjectDTO();
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.updateProject(null, project)
		);
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.updateProject("", project)
		);
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.updateProject(token1, null)
		);
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.updateProject(token1, project)
		);
	}

	@Test
	public void updateProjectOkTest() throws Exception {
		@NotNull final ProjectDTO project = new ProjectDTO();
		project.setName("project");
		@Nullable final ProjectDTO check = projectEndpoint.findOneProject(token1, project.getId());
		Assert.assertNull(check);
		@NotNull ProjectDTO created = projectEndpoint.createProject(token1, project);
		created.setName("testProject");
		projectEndpoint.updateProject(token1, created);
		@NotNull ProjectDTO found = projectEndpoint.findOneProject(token1, created.getId());
		Assert.assertNotNull(found);
		Assert.assertEquals(created.getId(), found.getId());
		Assert.assertEquals(user1.getId(), found.getUserId());
		Assert.assertEquals(created.getCreationDate(), found.getCreationDate());
		Assert.assertEquals(created.getName(), found.getName());
		created.setDescription("new Description");
		projectEndpoint.updateProject(token1, created);
		found = projectEndpoint.findOneProject(token1, created.getId());
		Assert.assertEquals(created.getDescription(), found.getDescription());
		created.setStatus(Status.IN_PROGRESS);
		projectEndpoint.updateProject(token1, created);
		found = projectEndpoint.findOneProject(token1, created.getId());
		Assert.assertEquals(created.getStatus(), found.getStatus());
	}

	@Test
	public void findOneProjectExceptionTest() {
		@NotNull final ProjectDTO project = new ProjectDTO();
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.findOneProject(null, project.getId())
		);
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.findOneProject("", project.getId())
		);
	}

	@Test
	public void findOneProjectNotFoundTest() throws Exception {
		@NotNull final ProjectDTO project1 = new ProjectDTO();
		project1.setName("project");
		projectEndpoint.createProject(token1, project1);
		@NotNull final ProjectDTO project2 = new ProjectDTO();
		project2.setName("project");
		projectEndpoint.createProject(token2, project2);
		@Nullable final ProjectDTO found1 = projectEndpoint.findOneProject(token1, project2.getId());
		Assert.assertNull(found1);
		@Nullable final ProjectDTO found2 = projectEndpoint.findOneProject(token2, project1.getId());
		Assert.assertNull(found2);
		@Nullable final ProjectDTO foundNull = projectEndpoint.findOneProject(token1, null);
		Assert.assertNull(foundNull);
		@Nullable final ProjectDTO foundEmptyId = projectEndpoint.findOneProject(token1, "");
		Assert.assertNull(foundEmptyId);
	}

	@Test
	public void findOneProjectOkTest() throws Exception {
		@NotNull final ProjectDTO project = new ProjectDTO();
		project.setName("project");
		@NotNull final ProjectDTO created = projectEndpoint.createProject(token1, project);
		@Nullable final ProjectDTO found = projectEndpoint.findOneProject(token1, created.getId());
		Assert.assertNotNull(found);
		Assert.assertEquals(project.getName(), found.getName());
	}

	@Test
	public void findOneProjectByNameExceptionTest() {
		@NotNull final ProjectDTO project = new ProjectDTO();
		project.setName("project");
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.findProjectByName(null, project.getName())
		);
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.findProjectByName("", project.getName())
		);
	}

	@Test
	public void findProjectByNameNotFoundTest() throws Exception {
		for (int i = 0; i < 11; i++) {
			@NotNull final ProjectDTO project = new ProjectDTO();
			project.setName("project" + i);
			projectEndpoint.createProject(token1, project);
		}
		@Nullable final List<ProjectDTO> found = projectEndpoint.findProjectByName(token1, "t11");
		Assert.assertEquals(0, found.size());
		@Nullable final List<ProjectDTO> found2 = projectEndpoint.findProjectByName(token1, null);
		Assert.assertEquals(0, found2.size());
		@Nullable final List<ProjectDTO> found3 = projectEndpoint.findProjectByName(token1, "");
		Assert.assertEquals(0, found3.size());
		@Nullable final List<ProjectDTO> found4 = projectEndpoint.findProjectByName(token2, "1");
		Assert.assertEquals(0, found4.size());
	}

	@Test
	public void findProjectByNameOkTest() throws Exception {
		for (int i = 0; i < 11; i++) {
			@NotNull final ProjectDTO project = new ProjectDTO();
			project.setName("project" + i);
			projectEndpoint.createProject(token1, project);
		}
		@Nullable final List<ProjectDTO> found = projectEndpoint.findProjectByName(token1, "t1");
		Assert.assertEquals(2, found.size());
		@Nullable final List<ProjectDTO> found2 = projectEndpoint.findProjectByName(token1, "t7");
		Assert.assertEquals(1, found2.size());
	}

	@Test
	public void findOneProjectByDescriptionExceptionTest() {
		@NotNull final ProjectDTO project = new ProjectDTO();
		project.setDescription("description");
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.findProjectByDescription(null, project.getDescription())
		);
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.findProjectByDescription("", project.getDescription())
		);
	}

	@Test
	public void findProjectByDescriptionNotFoundTest() throws Exception {
		for (int i = 0; i < 11; i++) {
			@NotNull final ProjectDTO project = new ProjectDTO();
			project.setName("project" + i);
			project.setDescription("description" + i);
			projectEndpoint.createProject(token1, project);
		}
		@Nullable final List<ProjectDTO> found = projectEndpoint.findProjectByDescription(token1, "n11");
		Assert.assertEquals(0, found.size());
		@Nullable final List<ProjectDTO> found1 = projectEndpoint.findProjectByDescription(token1, "");
		Assert.assertEquals(0, found1.size());
		@Nullable final List<ProjectDTO> found2 = projectEndpoint.findProjectByDescription(token1, null);
		Assert.assertEquals(0, found2.size());
		@Nullable final List<ProjectDTO> found3 = projectEndpoint.findProjectByDescription(token2, "1");
		Assert.assertEquals(0, found3.size());
	}

	@Test
	public void findProjectByDescriptionOkTest() throws Exception {
		for (int i = 0; i < 11; i++) {
			@NotNull final ProjectDTO project = new ProjectDTO();
			project.setName("project" + i);
			project.setDescription("description" + i);
			projectEndpoint.createProject(token1, project);
		}
		@Nullable final List<ProjectDTO> found = projectEndpoint.findProjectByDescription(token1, "n1");
		Assert.assertEquals(2, found.size());
		@Nullable final List<ProjectDTO> found2 = projectEndpoint.findProjectByDescription(token1, "n7");
		Assert.assertEquals(1, found2.size());
		@Nullable final List<ProjectDTO> found1 = projectEndpoint.findProjectByDescription(token1, "");
		Assert.assertEquals(0, found1.size());
	}

	@Test
	public void findOneProjectByNameOrDescExceptionTest() {
		@NotNull final ProjectDTO project = new ProjectDTO();
		project.setName("project");
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.findProjectByNameOrDesc(null, project.getName())
		);
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.findProjectByNameOrDesc("", project.getName())
		);
	}

	@Test
	public void findProjectByNameOrDescNotFoundTest() throws Exception {
		for (int i = 0; i < 11; i++) {
			@NotNull final ProjectDTO project = new ProjectDTO();
			project.setName("project second" + i);
			project.setDescription("description" + i);
			projectEndpoint.createProject(token1, project);
		}
		@Nullable final List<ProjectDTO> found = projectEndpoint.findProjectByNameOrDesc(token1, "11");
		Assert.assertEquals(0, found.size());
		@Nullable final List<ProjectDTO> found1 = projectEndpoint.findProjectByNameOrDesc(token1, "");
		Assert.assertEquals(0, found1.size());
		@Nullable final List<ProjectDTO> found2 = projectEndpoint.findProjectByNameOrDesc(token1, null);
		Assert.assertEquals(0, found2.size());
		@Nullable final List<ProjectDTO> found3 = projectEndpoint.findProjectByNameOrDesc(token2, "1");
		Assert.assertEquals(0, found3.size());
	}

	@Test
	public void findProjectByNameOrDescOkTest() throws Exception {
		for (int i = 0; i < 11; i++) {
			@NotNull final ProjectDTO project = new ProjectDTO();
			project.setName("project" + i);
			project.setDescription("description" + i);
			projectEndpoint.createProject(token1, project);
		}
		@Nullable final List<ProjectDTO> found = projectEndpoint.findProjectByNameOrDesc(token1, "t1");
		Assert.assertEquals(2, found.size());
		@Nullable final List<ProjectDTO> found1 = projectEndpoint.findProjectByNameOrDesc(token1, "t7");
		Assert.assertEquals(1, found1.size());
		@Nullable final List<ProjectDTO> found2 = projectEndpoint.findProjectByNameOrDesc(token1, "n1");
		Assert.assertEquals(2, found2.size());
		@Nullable final List<ProjectDTO> found3 = projectEndpoint.findProjectByNameOrDesc(token1, "n7");
		Assert.assertEquals(1, found3.size());
	}

	@Test
	public void findAllProjectsExceptionTest() {
		@NotNull final String comparator = "name";
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.findAllProjects(null, comparator)
		);
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.findProjectByNameOrDesc("", comparator)
		);
	}

	@Test
	public void findAllProjectsTest() throws Exception {
		@NotNull final String comparator = "name";
		@NotNull final List<ProjectDTO> found = projectEndpoint.findAllProjects(token1, comparator);
		Assert.assertEquals(0, found.size());
		for (int i = 0; i < 11; i++) {
			@NotNull final ProjectDTO project = new ProjectDTO();
			project.setName("project" + i);
			projectEndpoint.createProject(token1, project);
		}
		@NotNull final List<ProjectDTO> found1 = projectEndpoint.findAllProjects(token1, comparator);
		Assert.assertEquals(11, found1.size());
		@NotNull final List<ProjectDTO> found2 = projectEndpoint.findAllProjects(token2, comparator);
		Assert.assertEquals(0, found2.size());
	}

	@Test
	public void findAllProjectsSortTest() throws Exception {
		@NotNull final ProjectDTO project1 = new ProjectDTO();
		project1.setName("project1");
		project1.setStartDate(DateUtil.parseDate("11.12.2012"));
		@NotNull final ProjectDTO project2 = new ProjectDTO();
		project2.setName("project3");
		project2.setStatus(Status.DONE);
		@NotNull final ProjectDTO project3 = new ProjectDTO();
		project3.setName("project2");
		project3.setFinishDate(DateUtil.parseDate("11.12.2012"));
		projectEndpoint.createProject(token1, project1);
		Thread.sleep(1000);
		projectEndpoint.createProject(token1, project2);
		Thread.sleep(1000);
		projectEndpoint.createProject(token1, project3);
		@NotNull final List<ProjectDTO> found = projectEndpoint.findAllProjects(token1, null);
		Assert.assertEquals("project1", found.get(0).getName());
		Assert.assertEquals("project2", found.get(1).getName());
		Assert.assertEquals("project3", found.get(2).getName());
		@NotNull final List<ProjectDTO> found1 = projectEndpoint.findAllProjects(token1, "creationDate");
		Assert.assertEquals("project1", found1.get(0).getName());
		Assert.assertEquals("project3", found1.get(1).getName());
		Assert.assertEquals("project2", found1.get(2).getName());
		@NotNull final List<ProjectDTO> found2 = projectEndpoint.findAllProjects(token1, "startDate");
		Assert.assertEquals("project1", found2.get(2).getName());
		@NotNull final List<ProjectDTO> found3 = projectEndpoint.findAllProjects(token1, "finishDate");
		Assert.assertEquals("project2", found3.get(2).getName());
		@NotNull final List<ProjectDTO> found4 = projectEndpoint.findAllProjects(token1, "status");
		Assert.assertEquals("project3", found4.get(0).getName());
	}

	@Test
	public void removeAllProjectsExceptionTest() {
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.removeAllProjects(null)
		);
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.removeAllProjects("")
		);
	}

	@Test
	public void removeAllProjectsOkTest() throws Exception {
		for (int i = 0; i < 11; i++) {
			@NotNull final ProjectDTO project = new ProjectDTO();
			project.setName("project" + i);
			projectEndpoint.createProject(token1, project);
		}
		projectEndpoint.removeAllProjects(token2);
		@NotNull final List<ProjectDTO> found = projectEndpoint.findAllProjects(token1, "name");
		Assert.assertNotEquals(0, found.size());
		projectEndpoint.removeAllProjects(token1);
		@NotNull final List<ProjectDTO> found2 = projectEndpoint.findAllProjects(token1, "name");
		Assert.assertEquals(0, found2.size());
	}

	@Test
	public void removeProjectExceptionTest() {
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.removeProject(null, "")
		);
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.removeProject("", "")
		);
	}

	@Test
	public void removeProjectTest() throws Exception {
		@NotNull final ProjectDTO project = new ProjectDTO();
		project.setName("project");
		@NotNull final ProjectDTO created = projectEndpoint.createProject(token1, project);
		@Nullable final ProjectDTO found = projectEndpoint.findOneProject(token1, created.getId());
		Assert.assertNotNull(found);
		Assert.assertThrows(
				Exception.class,
				() -> projectEndpoint.removeProject(token2, created.getId())
		);
		projectEndpoint.removeProject(token1, created.getId());
		@Nullable final ProjectDTO foundRemoved = projectEndpoint.findOneProject(token1, created.getId());
		Assert.assertNull(foundRemoved);
	}

}
