https://gitlab.com/ahmetahunov/sp-01
# TASK MANAGER

## SOFTWARE:
+ Git
+ JRE
+ Java 8
+ Maven 4.0
+ PostgreSQL

## Developer

  Rustamzhan Akhmetakhunov\
  email: ahmetahunov@yandex.ru

## build app

```bash
git clone http://gitlab.volnenko.school/ahmetahunov/sp-01.git
cd sp-01
mvn clean install
```

## run app server
```bash
java -jar tm-server/target/release/bin/tm-server.jar
```

## run app client
```bash
java -jar tm-client/target/release/bin/tm-client.jar
```

## open server docs in browser
#### windows
```
start tm-server\target\release\docs\apidocs\index.html
```

#### macOs
```
open tm-client/target/release/docs/apidocs/index.html
```

#### linux
```
xdg-open tm-client/target/release/docs/apidocs/index.html
```