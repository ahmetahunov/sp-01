package ru.ahmetahunov.sp.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.ahmetahunov.sp.api.repository.ISessionRepository;
import ru.ahmetahunov.sp.api.service.ISessionService;
import ru.ahmetahunov.sp.dto.SessionDTO;
import ru.ahmetahunov.sp.entity.Session;
import ru.ahmetahunov.sp.enumerated.Role;
import ru.ahmetahunov.sp.exception.AccessForbiddenException;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import ru.ahmetahunov.sp.util.SessionSignatureUtil;
import java.util.Collections;
import java.util.List;

@Transactional
public class SessionService implements ISessionService {

	@Setter
	@NotNull
	private ISessionRepository repository;

	@Override
	public void persist(@Nullable final Session session) {
		if (session == null) return;
		repository.persist(session);
	}

	@Override
	public void merge(@Nullable final Session session) {
		if (session == null) return;
		repository.merge(session);
	}

	@Nullable
	@Override
	public Session findOne(@Nullable final String id, @Nullable final String userId) {
		if (id == null || id.isEmpty()) return null;
		if (userId == null || userId.isEmpty()) return null;
		return repository.findOneByUserId(id, userId);
	}

	@Nullable
	@Override
	public Session findOne(@Nullable final String id) {
		if (id == null || id.isEmpty()) return null;
		return repository.findOne(id);
	}

	@NotNull
	@Override
	public List<Session> findAll() {
		return repository.findAll();
	}

	@NotNull
	@Override
	public List<Session> findAll(@Nullable final String userId) {
		if (userId == null || userId.isEmpty()) return Collections.emptyList();
		return repository.findAll(userId);
	}

	@Override
	public void remove(@Nullable final String id, @Nullable final String userId) throws InterruptedOperationException {
		if (id == null || id.isEmpty()) return;
		if (userId == null || userId.isEmpty()) return;
		repository.removeByUserId(id, userId);
	}

	@Override
	public void remove(@Nullable final String id) throws InterruptedOperationException {
		if (id == null || id.isEmpty()) return;
		repository.remove(id);
	}

	@Override
	public void validate(@Nullable final SessionDTO session)
			throws AccessForbiddenException, InterruptedOperationException {
		if (session == null) throw new AccessForbiddenException();
		@Nullable final Session checkSession = repository.findOne(session.getId());
		if (checkSession == null || checkSession.getSignature() == null)
			throw new AccessForbiddenException();
		@Nullable final String signature = session.getSignature();
		session.setSignature(null);
		@Nullable final String hash = SessionSignatureUtil.sign(session);
		if (!checkSession.getSignature().equals(hash) || !signature.equals(hash))
			throw new AccessForbiddenException();
		final long existTime = System.currentTimeMillis() - session.getTimestamp();
		if (existTime > 32400000) {
			repository.remove(session.getId());
			throw new AccessForbiddenException("Access denied! Session has expired.");
		}
	}

	@Override
	public void validate(@Nullable final SessionDTO session, @NotNull final Role role)
			throws AccessForbiddenException, InterruptedOperationException {
		validate(session);
		if (!role.equals(session.getRole())) throw new AccessForbiddenException();
	}

}
