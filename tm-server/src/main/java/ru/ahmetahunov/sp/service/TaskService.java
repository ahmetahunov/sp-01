package ru.ahmetahunov.sp.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.ahmetahunov.sp.api.repository.ITaskRepository;
import ru.ahmetahunov.sp.api.service.ITaskService;
import ru.ahmetahunov.sp.entity.Task;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import ru.ahmetahunov.sp.util.ComparatorUtil;
import java.util.Collections;
import java.util.List;

@Transactional
public class TaskService implements ITaskService {

    @Setter
    @NotNull
    private ITaskRepository repository;

    @Override
    public void persist(@Nullable final Task task) throws InterruptedOperationException {
        if (task == null) throw new InterruptedOperationException();
        repository.persist(task);
    }

    @Override
    public void merge(@Nullable final Task task) throws InterruptedOperationException {
        if (task == null) throw new InterruptedOperationException();
        repository.merge(task);
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findOne(id);
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        return repository.findOneById(userId, taskId);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId, @Nullable String comparator) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findAllWithComparator(userId, ComparatorUtil.getComparator(comparator));
    }

    @NotNull
    @Override
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable String comparator
    ) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId, ComparatorUtil.getComparator(comparator));
    }

    @NotNull
    @Override
    public List<Task> findByName(@Nullable final String userId, @Nullable final String taskName) {
        if (taskName == null || taskName.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findByName(userId, "%" + taskName + "%");
    }

    @NotNull
    @Override
    public List<Task> findByDescription(@Nullable final String userId, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (description == null || description.isEmpty()) return Collections.emptyList();
        return repository.findByDescription(userId, "%" + description + "%");
    }

    @NotNull
    @Override
    public List<Task> findByNameOrDesc(@Nullable final String userId, @Nullable final String searchPhrase) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (searchPhrase == null || searchPhrase.isEmpty()) return Collections.emptyList();
        return repository.findByNameOrDesc(userId, "%" + searchPhrase + "%");
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskId) throws InterruptedOperationException {
        if (taskId == null || taskId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        repository.removeById(userId, taskId);
    }

    @Override
    public void remove(@Nullable final String id) throws InterruptedOperationException {
        if (id == null || id.isEmpty()) return;
        repository.remove(id);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        repository.removeAll(userId);
    }

}
