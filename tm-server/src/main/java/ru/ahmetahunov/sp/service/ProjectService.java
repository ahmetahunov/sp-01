package ru.ahmetahunov.sp.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.ahmetahunov.sp.api.repository.IProjectRepository;
import ru.ahmetahunov.sp.api.service.IProjectService;
import ru.ahmetahunov.sp.entity.Project;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import ru.ahmetahunov.sp.util.ComparatorUtil;
import java.util.Collections;
import java.util.List;

@Transactional
public class ProjectService implements IProjectService {

    @Setter
    @NotNull
    private IProjectRepository repository;

    @Override
    public void persist(@Nullable final Project project) throws InterruptedOperationException {
        if (project == null) throw new InterruptedOperationException();
        if (project.getName().isEmpty()) throw new InterruptedOperationException();
        repository.persist(project);
    }

    @Override
    public void merge(@Nullable final Project project) throws InterruptedOperationException {
        if (project == null) throw new InterruptedOperationException();
        if (project.getName().isEmpty()) throw new InterruptedOperationException();
        repository.merge(project);
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.findOneByUserId(userId, id);
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findOne(id);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable String comparator) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findAllWithComparator(userId, ComparatorUtil.getComparator(comparator));
    }

    @NotNull
    @Override
    public List<Project> findByName(@Nullable final String userId, @Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findByName(userId, "%" + projectName + "%");
    }

    @NotNull
    @Override
    public List<Project> findByDescription(@Nullable final String userId, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (description == null || description.isEmpty()) return Collections.emptyList();
        return repository.findByDescription(userId, "%" + description + "%");
    }

    @NotNull
    @Override
    public List<Project> findByNameOrDesc(@Nullable final String userId, @Nullable final String searchPhrase) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (searchPhrase == null || searchPhrase.isEmpty()) return Collections.emptyList();
        return repository.findByNameOrDesc(userId, "%" + searchPhrase + "%");
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectId) throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        repository.remove(userId, projectId);
    }

    @Override
    public void remove(@Nullable final String id) throws InterruptedOperationException {
        if (id == null || id.isEmpty()) return;
        repository.removeById(id);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        repository.removeAll(userId);
    }

}
