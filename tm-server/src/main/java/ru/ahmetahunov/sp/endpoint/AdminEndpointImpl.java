package ru.ahmetahunov.sp.endpoint;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.sp.api.endpoint.AdminEndpoint;
import ru.ahmetahunov.sp.api.service.ISessionService;
import ru.ahmetahunov.sp.api.service.IUserService;
import ru.ahmetahunov.sp.dto.SessionDTO;
import ru.ahmetahunov.sp.dto.UserDTO;
import ru.ahmetahunov.sp.entity.User;
import ru.ahmetahunov.sp.enumerated.Role;
import ru.ahmetahunov.sp.exception.AccessForbiddenException;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import ru.ahmetahunov.sp.util.CipherUtil;
import ru.ahmetahunov.sp.util.PassUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.util.ArrayList;
import java.util.List;

@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.ahmetahunov.sp.api.endpoint.AdminEndpoint")
public final class AdminEndpointImpl implements AdminEndpoint {

	@NotNull
	private ISessionService sessionService;

	@NotNull
	private IUserService userService;

	@NotNull
	private String secretPhrase;

	@NotNull
	private String endpointHost;

	@NotNull
	private String endpointPort;

	public void init() {
		@NotNull final String host = String.format(
				"http://%s:%s/AdminEndpoint?wsdl",
				endpointHost,
				endpointPort
		);
		Endpoint.publish(host, this);
		System.out.println(host + " is running...");
	}

	@NotNull
	@Override
	@WebMethod
	public List<UserDTO> findAllUsers(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO session = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(session, Role.ADMINISTRATOR);
		@NotNull final List<UserDTO> users = new ArrayList<>();
		for (@NotNull final User user : userService.findAll())
			users.add(user.transformToDTO());
		return users;
	}

	@Override
	@WebMethod
	public void userUpdatePasswordAdmin(
			@WebParam(name = "session") final String token,
			@WebParam(name = "userId") final String userId,
			@WebParam(name = "password") final String password
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO session = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(session, Role.ADMINISTRATOR);
		userService.updatePasswordAdmin(userId, password);
	}

	@Override
	@WebMethod
	public void userChangeRoleAdmin(
			@WebParam(name = "session") final String token,
			@WebParam(name = "userId") final String userId,
			@WebParam(name = "role") final Role role
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO session = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(session, Role.ADMINISTRATOR);
		userService.updateRole(userId, role);
	}

	@Override
	@WebMethod
	public void userRemove(
			@WebParam(name = "session") final String token,
			@WebParam(name = "userId") final String userId
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO session = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(session, Role.ADMINISTRATOR);
		userService.remove(userId);
	}

	@Override
	@WebMethod
	public UserDTO userRegisterAdmin(
			@WebParam(name = "session") final String token,
			@WebParam(name = "user") final String login,
			@WebParam(name = "password") final String password
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO session = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(session, Role.ADMINISTRATOR);
		@NotNull final User user = new User();
		user.setLogin(login);
		user.setPassword(PassUtil.getHash(password));
		userService.persist(user);
		@Nullable final User found = userService.findOne(user.getId());
		return (found == null) ? null : found.transformToDTO();
	}

	@Nullable
	@Override
	@WebMethod
	public UserDTO userFindByLogin(
			@WebParam(name = "session") final String token,
			@WebParam(name = "login") final String login
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO session = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(session, Role.ADMINISTRATOR);
		@Nullable final User user = userService.findUser(login);
		return (user == null) ? null : user.transformToDTO();
	}

}
