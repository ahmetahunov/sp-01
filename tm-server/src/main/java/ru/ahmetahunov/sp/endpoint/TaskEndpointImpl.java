package ru.ahmetahunov.sp.endpoint;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.sp.api.endpoint.TaskEndpoint;
import ru.ahmetahunov.sp.api.service.*;
import ru.ahmetahunov.sp.dto.SessionDTO;
import ru.ahmetahunov.sp.dto.TaskDTO;
import ru.ahmetahunov.sp.entity.Task;
import ru.ahmetahunov.sp.exception.AccessForbiddenException;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import ru.ahmetahunov.sp.util.CipherUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.util.ArrayList;
import java.util.List;

@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.ahmetahunov.sp.api.endpoint.TaskEndpoint")
public final class TaskEndpointImpl implements TaskEndpoint {

	@NotNull
	private ISessionService sessionService;

	@NotNull
	private ITaskService taskService;

	@NotNull
	private IUserService userService;

	@NotNull
	private IProjectService projectService;

	@NotNull
	private String secretPhrase;

	@NotNull
	private String endpointHost;

	@NotNull
	private String endpointPort;

	public void init() {
		@NotNull final String host = String.format(
				"http://%s:%s/TaskEndpoint?wsdl",
				endpointHost,
				endpointPort
		);
		Endpoint.publish(host, this);
		System.out.println(host + " is running...");
	}

	@Nullable
	@Override
	@WebMethod
	public TaskDTO createTask(
			@WebParam(name = "session") final String token,
			@WebParam(name = "task") final TaskDTO taskDTO
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		if (taskDTO == null) throw new InterruptedOperationException();
		taskDTO.setUserId(sessionDTO.getUserId());
		taskService.persist(taskDTO.transformToTask(userService, projectService));
		@Nullable final Task task = taskService.findOne(sessionDTO.getUserId(), taskDTO.getId());
		return (task == null) ? null : task.transformToDTO();
	}

	@Nullable
	@Override
	@WebMethod
	public TaskDTO updateTask(
			@WebParam(name = "session") final String token,
			@WebParam(name = "task") final TaskDTO taskDTO
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		if (taskDTO == null) throw new InterruptedOperationException();
		taskDTO.setUserId(sessionDTO.getUserId());
		taskService.merge(taskDTO.transformToTask(userService, projectService));
		@Nullable final Task task = taskService.findOne(sessionDTO.getUserId(), taskDTO.getId());
		return (task == null) ? null : task.transformToDTO();
	}

	@Nullable
	@Override
	@WebMethod
	public TaskDTO findOneTask(
			@WebParam(name = "session") final String token,
			@WebParam(name = "taskId") final String taskId
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		@Nullable final Task task = taskService.findOne(sessionDTO.getUserId(), taskId);
		return (task == null) ? null : task.transformToDTO();
	}

	@NotNull
	@Override
	@WebMethod
	public List<TaskDTO> findTaskByName(
			@WebParam(name = "session") final String token,
			@WebParam(name = "taskName") final String taskName
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		@NotNull final List<Task> tasks = taskService.findByName(sessionDTO.getUserId(), taskName);
		@NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
		for (@NotNull final Task task : tasks) { tasksDTO.add(task.transformToDTO()); }
		return tasksDTO;
	}

	@NotNull
	@Override
	@WebMethod
	public List<TaskDTO> findTaskByDescription(
			@WebParam(name = "session") final String token,
			@WebParam(name = "description") final String description
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		@NotNull final List<Task> tasks = taskService.findByDescription(sessionDTO.getUserId(), description);
		@NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
		for (@NotNull final Task task : tasks) { tasksDTO.add(task.transformToDTO()); }
		return tasksDTO;
	}

	@NotNull
	@Override
	@WebMethod
	public List<TaskDTO> findTaskByNameOrDesc(
			@WebParam(name = "session") final String token,
			@WebParam(name = "searchPhrase") final String searchPhrase
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		@NotNull final List<Task> tasks = taskService.findByNameOrDesc(sessionDTO.getUserId(), searchPhrase);
		@NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
		for (@NotNull final Task task : tasks) { tasksDTO.add(task.transformToDTO()); }
		return tasksDTO;
	}

	@NotNull
	@Override
	@WebMethod
	public List<TaskDTO> findAllTasks(
			@WebParam(name = "session") final String token,
			@WebParam(name = "comparatorName") final String comparatorName
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		@NotNull final List<Task> tasks = taskService.findAll(sessionDTO.getUserId(), comparatorName);
		@NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
		for (@NotNull final Task task : tasks) { tasksDTO.add(task.transformToDTO()); }
		return tasksDTO;
	}

	@NotNull
	@Override
	@WebMethod
	public List<TaskDTO> findAllProjectTasks(
			@WebParam(name = "session") final String token,
			@WebParam(name = "projectId") final String projectId
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		@NotNull final List<Task> tasks = taskService.findAll(sessionDTO.getUserId(), projectId, "name");
		@NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
		for (@NotNull final Task task : tasks) { tasksDTO.add(task.transformToDTO()); }
		return tasksDTO;
	}

	@Override
	@WebMethod
	public void removeAllTasks(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		taskService.removeAll(sessionDTO.getUserId());
	}

	@Override
	@WebMethod
	public void removeTask(
			@WebParam(name = "session") final String token,
			@WebParam(name = "taskId") final String taskId
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		taskService.remove(sessionDTO.getUserId(), taskId);
	}

}
