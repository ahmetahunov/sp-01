package ru.ahmetahunov.sp.endpoint;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.sp.api.endpoint.ProjectEndpoint;
import ru.ahmetahunov.sp.api.service.*;
import ru.ahmetahunov.sp.dto.ProjectDTO;
import ru.ahmetahunov.sp.dto.SessionDTO;
import ru.ahmetahunov.sp.entity.Project;
import ru.ahmetahunov.sp.exception.AccessForbiddenException;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import ru.ahmetahunov.sp.util.CipherUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.util.ArrayList;
import java.util.List;

@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.ahmetahunov.sp.api.endpoint.ProjectEndpoint")
public final class ProjectEndpointImpl implements ProjectEndpoint {

	@NotNull
	private IUserService userService;

	@NotNull
	private ITaskService taskService;

	@NotNull
	private IProjectService projectService;

	@NotNull
	private ISessionService sessionService;

	@NotNull
	private String secretPhrase;

	@NotNull
	private String endpointHost;

	@NotNull
	private String endpointPort;

	public void init() {
		@NotNull final String host = String.format(
				"http://%s:%s/ProjectEndpoint?wsdl",
				endpointHost,
				endpointPort
		);
		Endpoint.publish(host, this);
		System.out.println(host + " is running...");
	}

	@Nullable
	@Override
	@WebMethod
	public ProjectDTO createProject(
			@WebParam(name = "session") final String token,
			@WebParam(name = "project") final ProjectDTO projectDTO
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		if (projectDTO == null) throw new InterruptedOperationException();
		projectDTO.setUserId(sessionDTO.getUserId());
		projectService.persist(projectDTO.transformToEntity(userService, taskService));
		@Nullable final Project project = projectService.findOne(sessionDTO.getUserId(), projectDTO.getId());
		return (project == null) ? null : project.transformToDTO();
	}

	@Nullable
	@Override
	@WebMethod
	public ProjectDTO updateProject(
			@WebParam(name = "session") final String token,
			@WebParam(name = "project") final ProjectDTO projectDTO
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		if (projectDTO == null) throw new InterruptedOperationException();
		projectDTO.setUserId(sessionDTO.getUserId());
		projectService.merge(projectDTO.transformToEntity(userService, taskService));
		@Nullable final Project project = projectService.findOne(sessionDTO.getUserId(), projectDTO.getId());
		return (project == null) ? null : project.transformToDTO();
	}

	@Nullable
	@Override
	@WebMethod
	public ProjectDTO findOneProject(
			@WebParam(name = "session") final String token,
			@WebParam(name = "projectId") final String projectId
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		@Nullable final Project project = projectService.findOne(sessionDTO.getUserId(), projectId);
		return (project == null) ? null : project.transformToDTO();
	}

	@NotNull
	@Override
	@WebMethod
	public List<ProjectDTO> findProjectByName(
			@WebParam(name = "session") final String token,
			@WebParam(name = "projectName") final String projectName
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		@NotNull final List<Project> projects = projectService.findByName(sessionDTO.getUserId(), projectName);
		@NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
		for (@NotNull final Project project : projects) { projectsDTO.add(project.transformToDTO()); }
		return projectsDTO;
	}

	@NotNull
	@Override
	@WebMethod
	public List<ProjectDTO> findProjectByDescription(
			@WebParam(name = "session") final String token,
			@WebParam(name = "description") final String description
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		@NotNull final List<Project> projects = projectService.findByDescription(sessionDTO.getUserId(), description);
		@NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
		for (@NotNull final Project project : projects) { projectsDTO.add(project.transformToDTO()); }
		return projectsDTO;
	}

	@NotNull
	@Override
	@WebMethod
	public List<ProjectDTO> findProjectByNameOrDesc(
			@WebParam(name = "session") final String token,
			@WebParam(name = "searchPhrase") final String searchPhrase
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		@NotNull final List<Project> projects = projectService.findByNameOrDesc(sessionDTO.getUserId(), searchPhrase);
		@NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
		for (@NotNull final Project project : projects) { projectsDTO.add(project.transformToDTO()); }
		return projectsDTO;
	}

	@NotNull
	@Override
	@WebMethod
	public List<ProjectDTO> findAllProjects(
			@WebParam(name = "session") final String token,
			@WebParam(name = "comparatorName") final String comparatorName
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		@NotNull final List<Project> projects = projectService.findAll(sessionDTO.getUserId(), comparatorName);
		@NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
		for (@NotNull final Project project : projects) { projectsDTO.add(project.transformToDTO()); }
		return projectsDTO;
	}

	@Override
	@WebMethod
	public void removeAllProjects(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		projectService.removeAll(sessionDTO.getUserId());
	}

	@Override
	@WebMethod
	public void removeProject(
			@WebParam(name = "session") final String token,
			@WebParam(name = "projectId") final String projectId
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		projectService.remove(sessionDTO.getUserId(), projectId);
	}

}
