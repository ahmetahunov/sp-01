package ru.ahmetahunov.sp.endpoint;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.sp.api.endpoint.UserEndpoint;
import ru.ahmetahunov.sp.api.service.ISessionService;
import ru.ahmetahunov.sp.api.service.IUserService;
import ru.ahmetahunov.sp.dto.SessionDTO;
import ru.ahmetahunov.sp.dto.UserDTO;
import ru.ahmetahunov.sp.entity.User;
import ru.ahmetahunov.sp.exception.AccessForbiddenException;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import ru.ahmetahunov.sp.util.CipherUtil;
import ru.ahmetahunov.sp.util.PassUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.ahmetahunov.sp.api.endpoint.UserEndpoint")
public final class UserEndpointImpl implements UserEndpoint {

	@NotNull
	private ISessionService sessionService;

	@NotNull
	private IUserService userService;

	@NotNull
	private String secretPhrase;

	@NotNull
	private String endpointHost;

	@NotNull
	private String endpointPort;

	public void init() {
		@NotNull final String host = String.format(
				"http://%s:%s/UserEndpoint?wsdl",
				endpointHost,
				endpointPort
		);
		Endpoint.publish(host, this);
		System.out.println(host + " is running...");
	}

	@Nullable
	@Override
	@WebMethod
	public UserDTO createUser(
			@WebParam(name = "login") final String login,
			@WebParam(name = "password") final String password
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final User user = new User();
		if (login != null) user.setLogin(login);
		user.setPassword(PassUtil.getHash(password));
		userService.persist(user);
		@Nullable final User found = userService.findOne(user.getId());
		return (found == null) ? null : found.transformToDTO();
	}

	@Override
	@WebMethod
	public void updatePassword(
			@WebParam(name = "session") final String token,
			@WebParam(name = "old") final String oldPassword,
			@WebParam(name = "password") final String password
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		userService.updatePassword(sessionDTO.getUserId(), oldPassword, password);
	}

	@Override
	@WebMethod
	public void updateLogin(
			@WebParam(name = "session") final String token,
			@WebParam(name = "login") final String login
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		userService.updateLogin(sessionDTO.getUserId(), login);
	}

	@Override
	@WebMethod
	public UserDTO findUser(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, InterruptedOperationException {
		@Nullable final SessionDTO sessionDTO = CipherUtil.decrypt(token, secretPhrase);
		sessionService.validate(sessionDTO);
		@Nullable final User user = userService.findOne(sessionDTO.getUserId());
		return (user == null) ? null : user.transformToDTO();
	}

}
