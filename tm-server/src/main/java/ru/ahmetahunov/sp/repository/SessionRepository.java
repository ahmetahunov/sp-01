package ru.ahmetahunov.sp.repository;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.ahmetahunov.sp.api.repository.ISessionRepository;
import ru.ahmetahunov.sp.entity.Session;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@NoArgsConstructor
public class SessionRepository implements ISessionRepository {

	@NotNull
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void persist(@NotNull final Session session) {
		entityManager.persist(session);
	}

	@Override
	public void merge(@NotNull final Session session) {
		entityManager.merge(session);
	}

	@Nullable
	@Override
	public Session findOne(@NotNull final String id) {
		@NotNull final TypedQuery<Session> query = entityManager.createQuery(
				"SELECT s FROM Session s WHERE s.id = :id",
				Session.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		query.setParameter("id", id);
		@NotNull final List<Session> sessions = query.getResultList();
		return (sessions.isEmpty()) ? null : sessions.get(0);
	}

	@Nullable
	@Override
	public Session findOneByUserId(@NotNull final String id, @NotNull final String userId) {
		@NotNull final TypedQuery<Session> query = entityManager.createQuery(
				"SELECT s FROM Session s WHERE s.user.id = :userId AND s.id = :id",
				Session.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		query.setParameter("userId", userId);
		query.setParameter("id", id);
		@NotNull final List<Session> sessions = query.getResultList();
		return (sessions.isEmpty()) ? null : sessions.get(0);
	}

	@NotNull
	@Override
	public List<Session> findAll() {
		@NotNull final TypedQuery<Session> query = entityManager.createQuery(
				"SELECT s FROM Session s",
				Session.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		return query.getResultList();
	}

	@NotNull
	@Override
	public List<Session> findAll(@NotNull final String userId) {
		@NotNull final TypedQuery<Session> query = entityManager.createQuery(
				"SELECT s FROM Session s WHERE s.user.id = :userId",
				Session.class
		);
		query.setHint(QueryHints.CACHEABLE, true);
		query.setParameter("userId", userId);
		return query.getResultList();
	}

	@Override
	public void remove(@NotNull final String id) throws InterruptedOperationException {
		@Nullable final Session session = findOne(id);
		if (session == null) throw new InterruptedOperationException();
		entityManager.remove(session);
	}

	@Override
	public void removeByUserId(@NotNull final String id, @NotNull final String userId) throws InterruptedOperationException {
		@Nullable final Session session = findOneByUserId(id, userId);
		if (session == null) throw new InterruptedOperationException();
		entityManager.remove(session);
	}

}
