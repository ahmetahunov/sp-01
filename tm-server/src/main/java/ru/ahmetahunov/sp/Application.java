package ru.ahmetahunov.sp;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public final class Application {

    public static void main( final String[] args ) {
        System.setProperty("javax.xml.bind.JAXBContext", "com.sun.xml.internal.bind.v2.ContextFactory");
        @NotNull final ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    }

}
