package ru.ahmetahunov.sp.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.sp.api.service.ITaskService;
import ru.ahmetahunov.sp.api.service.IUserService;
import ru.ahmetahunov.sp.entity.Project;
import ru.ahmetahunov.sp.enumerated.Status;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@XmlType
@NoArgsConstructor
public final class ProjectDTO extends AbstractEntityDTO implements Serializable {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date startDate = new Date(0);

    @NotNull
    private Date finishDate = new Date(0);

    @NotNull
    private Date creationDate = new Date(System.currentTimeMillis());

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    private String userId = "";

    @NotNull
    public Project transformToEntity(
            @NotNull final IUserService userService,
            @NotNull final ITaskService taskService
    ) {
        @NotNull final Project project = new Project();
        project.setId(this.id);
        project.setName(this.name);
        project.setDescription(this.description);
        project.setStartDate(this.startDate);
        project.setFinishDate(this.finishDate);
        project.setCreationDate(this.creationDate);
        project.setStatus(this.status);
        project.setTasks(taskService.findAll(this.userId, this.id, "name"));
        project.setUser(userService.findOne(this.userId));
        return project;
    }

}
