package ru.ahmetahunov.sp.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.sp.dto.UserDTO;
import ru.ahmetahunov.sp.enumerated.Role;
import ru.ahmetahunov.sp.exception.AccessForbiddenException;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface AdminEndpoint {

	@NotNull
	@WebMethod
	public List<UserDTO> findAllUsers(
			@WebParam(name = "token") String token
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	public void userUpdatePasswordAdmin(
			@WebParam(name = "token") String token,
			@WebParam(name = "userId") String userId,
			@WebParam(name = "password") String password
	) throws AccessForbiddenException, InterruptedOperationException;

	@Nullable
	@WebMethod
	public UserDTO userRegisterAdmin(
			@WebParam(name = "token") String token,
			@WebParam(name = "user") String login,
			@WebParam(name = "password") String password
	) throws AccessForbiddenException, InterruptedOperationException;

	@Nullable
	@WebMethod
	public UserDTO userFindByLogin(
			@WebParam(name = "token") String token,
			@WebParam(name = "login") String login
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	public void userChangeRoleAdmin(
			@WebParam(name = "token") String token,
			@WebParam(name = "userId") String userId,
			@WebParam(name = "role") Role role
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	public void userRemove(
			@WebParam(name = "token") String token,
			@WebParam(name = "userId") String userId
	) throws AccessForbiddenException, InterruptedOperationException;

}
