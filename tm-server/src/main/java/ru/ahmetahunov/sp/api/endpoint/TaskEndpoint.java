package ru.ahmetahunov.sp.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.sp.dto.TaskDTO;
import ru.ahmetahunov.sp.exception.AccessForbiddenException;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface TaskEndpoint {

	@Nullable
	@WebMethod
	public TaskDTO createTask(
			@WebParam(name = "token") String token,
			@WebParam(name = "task") TaskDTO task
	) throws AccessForbiddenException, InterruptedOperationException;

	@Nullable
	@WebMethod
	public TaskDTO updateTask(
			@WebParam(name = "token") String token,
			@WebParam(name = "task") TaskDTO task
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<TaskDTO> findAllTasks(
			@WebParam(name = "token") String token,
			@WebParam(name = "comparator") String comparator
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<TaskDTO> findAllProjectTasks(
			@WebParam(name = "token") String token,
			@WebParam(name = "projectId") String projectId
	) throws AccessForbiddenException, InterruptedOperationException;

	@Nullable
	@WebMethod
	public TaskDTO findOneTask(
			@WebParam(name = "token") String token,
			@WebParam(name = "taskId") String taskId
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<TaskDTO> findTaskByName(
			@WebParam(name = "token") String token,
			@WebParam(name = "taskName") String taskName
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<TaskDTO> findTaskByDescription(
			@WebParam(name = "token") String token,
			@WebParam(name = "description") String description
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<TaskDTO> findTaskByNameOrDesc(
			@WebParam(name = "token") String token,
			@WebParam(name = "searchPhrase") String searchPhrase
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	public void removeAllTasks(
			@WebParam(name = "token") String token
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	public void removeTask(
			@WebParam(name = "token") String token,
			@WebParam(name = "taskId") String taskId
	) throws AccessForbiddenException, InterruptedOperationException;

}
