package ru.ahmetahunov.sp.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.sp.entity.AbstractEntity;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import java.util.List;

public interface IAbstractService<T extends AbstractEntity> {

    public void persist(T item) throws InterruptedOperationException;

    public void merge(T item) throws InterruptedOperationException;

    @Nullable
    public T findOne(String id);

    @NotNull
    public List<T> findAll();

    public void remove(String id) throws InterruptedOperationException;

}
