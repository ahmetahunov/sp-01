package ru.ahmetahunov.sp.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.sp.dto.SessionDTO;
import ru.ahmetahunov.sp.entity.Session;
import ru.ahmetahunov.sp.enumerated.Role;
import ru.ahmetahunov.sp.exception.AccessForbiddenException;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import java.util.List;

public interface ISessionService extends IAbstractService<Session> {

	@Nullable
	public Session findOne(String id, String userId);

	@NotNull
	public List<Session> findAll(@NotNull String userId);

	public void remove(String id, String userId) throws InterruptedOperationException;

	public void validate(SessionDTO session) throws AccessForbiddenException, InterruptedOperationException;

	public void validate(SessionDTO session, Role role) throws AccessForbiddenException, InterruptedOperationException;

}
